# Perl Squasher

Squash your Perl modules down into a single script.

A typical use case for Squasher is a project that has one main script and a
directory with libraries useful only to that script.

A good example of a project where this is the case is
[`App::Ack`](https://github.com/beyondgrep/ack3).

## More than `cat`

Squasher normally strips the following:

- Repeated newlines
- Pod inside Perl module files (`*.pm`)
- `## no critic` comments
- `1;` at the end of modules

## Usage

```sh
shopt -s globstar
squash main-script.pl lib/**/*.pm >script-all-in-one.pl
```

## Directives

Inside your code, you can use `:squash-...:` directives to control how lines
are removed or preserved.

Squasher will remove lines with these directive in it.

- `# :squash-remove-start:` / `# :squash-remove-end:`

Remove the whole section starting with the `squash-remove-start` directive line
and ending at the end of the `squash-remove-end` directive line.

- `# :squash-ignore-start:` / `# :squash-ignore-end:`

Strip the directive line, then pass without removing anything else until it
reaches the `squash-ignore-end` directive line.

This works for preserving Pod inside of module files, which Squasher normally
removes.

Both start and end directive lines are removed.

# Examples

A typical use case for Squasher:

`myscript.pl`:

```perl
#!/usr/bin/perl
use strict;
use warnings;
use Carp;
# ... more libraries

# :squash-remove-start:
# Prepend our local library dir to the search path.
use FindBin qw//;
use lib "$FindBin::RealBin/lib";

# Load our libraries
require MyLib;
# :squash-remove-end:

# Continue with the rest of the program
my $obj = MyLib->new;
# ...
```

`lib/MyLib.pm`:

```perl
package MyLib;
use strict;
use warnings;

=pod
This is how you use this module...
=cut

sub new {}

1;
```

Running `squash myscript.pl lib/MyLib.pm` will output the following program:

```perl
#!/usr/bin/perl
use strict;
use warnings;
use Carp;
# ... more libraries

# Continue with the rest of the program
my $obj = MyLib->new;
# ...
package MyLib;
use strict;
use warnings;

sub new {}
```

## Best Practices

- Surround `use lib ...` and `use [locally-loaded library]` invocations with `# :squash-remove-start:` / `# :squash-remove-end:`.
- If you have strings with repeated newlines, surround them with `# :squash-ignore-start:` / `# :squash-ignore-end:`.

## Thanks

This project was inspired and adapted from the script that squashes
[App::Ack](https://github.com/beyondgrep/ack3).
