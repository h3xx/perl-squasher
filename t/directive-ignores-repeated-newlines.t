#!perl
use strict;
use warnings;

use Carp qw/ croak /;
use Test::More 'no_plan';

use TestFunctions;

my $temp_dir = mktempdir();
my @script_lines = (
    '#!perl',
    'stripped_newlines_follow();',
    # Total of 2 newlines in output
    ('') x 8,
    'end_of_stripped_newlines();',
    '',
    '# :squash-ignore-start:',
    'allowed_newlines_follow();',
    # Total of 4 newlines in output
    ('') x 3,
    'end_of_allowed_newlines();',
    '# :squash-ignore-end:',
);
my @pm_lines = (
    'package MyLib;',
    'sub module_method {}',
);

my $mylib = "$temp_dir/MyLib.pm";
my $myscript = "$temp_dir/myscript.pl";

# Set up files
open my $pm, '>', $mylib
    or croak 'Cannot write to temp dir';
print $pm join "\n", @pm_lines;
close $pm;

open my $sc, '>', $myscript
    or croak 'Cannot write to temp dir';
print $sc join "\n", @script_lines;
close $sc;

my ($exit_code, $stdout, $stderr) = run_script_capture($myscript, $mylib);
is( $exit_code, 0, 'script should exit non-zero' );

# Smoke tests
like( $stdout, qr/#!perl$/m,
    'script should be created' );
like( $stdout, qr/sub module_method \{\}$/m,
    'module should provide subroutines' );
unlike( $stdout, qr/:squash/,
    'squash directives should be stripped' );
my $s_start = qr/stripped_newlines_follow\(\);/;
my $s_end = qr/end_of_stripped_newlines\(\);/;
like( $stdout, qr/$s_start\n\n$s_end/,
    'normal repeated newlines should not be allowed' );

# Topical tests
my $a_start = qr/allowed_newlines_follow\(\);/;
my $a_end = qr/end_of_allowed_newlines\(\);/;
like( $stdout, qr/$a_start\n\n\n\n$a_end/,
    'ignored sections should allow repeated newlines' );
