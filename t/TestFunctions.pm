package TestFunctions;
use strict;
use warnings;

use Cwd qw/
    abs_path
/;
use File::Basename qw/
    dirname
/;
require File::Temp;
use IPC::Open3 qw/ open3 /;
use Symbol qw/ gensym /;

use Exporter;
use parent 'Exporter';
## no critic ( Modules::ProhibitAutomaticExportation )
# This is a test function library, it's not production code...
our @EXPORT = qw/
    mktempdir
    run_script_capture
/;

use constant SCRIPT => $ENV{SCRIPT} // abs_path dirname(__FILE__) . '/../squash';

sub mktempdir {
    return File::Temp->newdir(
        TEMPLATE => 'tests.XXXXXX',
        TMPDIR => 1,
        CLEANUP => 1,
    );
}

sub run_script_capture {
    my @args = @_;
    my @cmd = (SCRIPT, @args);
    my $in = '';
    my $child_out = gensym();
    my $child_err = gensym();
    print STDERR "+ @cmd\n";
    my $pid = open3 $in, $child_out, $child_err, @cmd;
    waitpid $pid, 0;
    foreach my $handle ($child_out, $child_err) {
        seek $handle, 0, 0;
    }

    local $/;
    return (
        $?,
        scalar <$child_out>, # slurp!
        scalar <$child_err>, # slurp!
    );
}

1;
