#!perl
use strict;
use warnings;

use Carp qw/ croak /;
use Test::More 'no_plan';

use TestFunctions;

my $temp_dir = mktempdir();
my @script_lines = (
    '#!perl',
);
my @pm_lines = (
    'package MyLib;',
    '# :squash-ignore-start:',
    '=head1 This Pod not should be stripped',
    'some module pod',
    '=cut',
    '# :squash-ignore-end:',
    'sub module_method {}',
);

my $mylib = "$temp_dir/MyLib.pm";
my $myscript = "$temp_dir/myscript.pl";

# Set up files
open my $pm, '>', $mylib
    or croak 'Cannot write to temp dir';
print $pm join "\n", @pm_lines;
close $pm;

open my $sc, '>', $myscript
    or croak 'Cannot write to temp dir';
print $sc join "\n", @script_lines;
close $sc;

my ($exit_code, $stdout, $stderr) = run_script_capture($myscript, $mylib);
is( $exit_code, 0, 'script should exit non-zero' );

# Smoke tests
like( $stdout, qr/#!perl$/m,
    'script should be created' );
like( $stdout, qr/sub module_method \{\}$/m,
    'module should provide subroutines' );
unlike( $stdout, qr/:squash/,
    'squash directives should be stripped' );

# Topical tests
like( $stdout, qr/This Pod not should be stripped/,
    'pod inside ignored sections should come from module files' );
like( $stdout, qr/some module pod/,
    'pod inside ignored sections should come from module files' );
