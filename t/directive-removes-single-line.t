#!perl
use strict;
use warnings;

use Carp qw/ croak /;
use Test::More 'no_plan';

use TestFunctions;

my $temp_dir = mktempdir();
my @script_lines = (
    '#!perl',
    'ignored_line(); # :squash-remove-line:',
    'nonignored_line;',
);

my $myscript = "$temp_dir/myscript.pl";

# Set up files
open my $sc, '>', $myscript
    or croak 'Cannot write to temp dir';
print $sc join "\n", @script_lines;
close $sc;

my ($exit_code, $stdout, $stderr) = run_script_capture($myscript);
is( $exit_code, 0, 'script should exit non-zero' );

# Smoke tests
like( $stdout, qr/#!perl$/m,
    'script should be created' );
like( $stdout, qr/nonignored_line;/,
    'normal lines should not be stripped' );

# Topical tests
unlike( $stdout, qr/ignored_line\(\);/,
    'single removed lines should be stripped' );
