#!perl
use strict;
use warnings;

use Carp qw/ croak /;
use Test::More 'no_plan';

use TestFunctions;

my $temp_dir = mktempdir();
my @script_lines = (
    '#!perl',
    '# :squash-remove-start:',
    'use MyLib;',
    '# :squash-remove-end:',
);
my @pm_lines = (
    'package MyLib;',
    'sub module_method {}',
);

my $mylib = "$temp_dir/MyLib.pm";
my $myscript = "$temp_dir/myscript.pl";

# Set up files
open my $pm, '>', $mylib
    or croak 'Cannot write to temp dir';
print $pm join "\n", @pm_lines;
close $pm;

open my $sc, '>', $myscript
    or croak 'Cannot write to temp dir';
print $sc join "\n", @script_lines;
close $sc;

my ($exit_code, $stdout, $stderr) = run_script_capture($myscript, $mylib);
is( $exit_code, 0, 'script should exit non-zero' );

# Smoke tests
like( $stdout, qr/#!perl$/m,
    'script should be created' );
like( $stdout, qr/sub module_method \{\}$/m,
    'module should provide subroutines' );
unlike( $stdout, qr/:squash/,
    'squash directives should be stripped' );

# Topical tests
unlike( $stdout, qr/use MyLib;/,
    'remove sections should be stripped' );
