- Can't handle `__DATA__` sections.
- Can't handle `__END__`.
- The `__FILE__` constant changes when you squash.
- Can introduce "Bareword 'MyPackage::' refers to nonexistent package" warnings
  if `MyPackage::->method()` method call notation is used anywhere in the
  script before `package MyPackage;` occurs. It will still call the method,
  though.
